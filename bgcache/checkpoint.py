import gzip
import inspect

import dill

from os import makedirs
from os.path import exists, join, dirname


def bgcheckpoint(*variables, key="MAIN"):
    """

    Create a checkpoint of the given variables.

    Usage example:

    a, b, c = bgcheckpoint('a', 'b', 'c', key="classifier_key")
    
    :param variables:
    :param key:
    :return:
    """

    # Allow to use as decorator
    # if len(variables) == 0:
    #     raise RuntimeWarning("using as decorator with key argument")
    #
    # if len(variables) == 1 and key == "MAIN" and callable(variables[0]):
    #     raise RuntimeWarning("using as decorator without arguments")

    namespace = inspect.currentframe().f_back.f_locals

    result = []
    for variable in variables:
        if type(variable) != str:
            continue

        value = None
        file_key = join(".bgcheckpoint", key, "{}.gz".format(variable))
        if variable in namespace:

            value = namespace[variable]

            # Store it
            makedirs(dirname(file_key), exist_ok=True)
            with gzip.open(file_key, "wb") as fd:
                dill.dump(value, fd)

        else:

            if exists(file_key):
                with gzip.open(file_key, "rb") as fd:
                    value = dill.load(fd)
            else:
                raise ValueError("Variable '{}' is not defined neither checkpointed at disk.".format(variable))

        result.append(value)

    return tuple(result)