from bgcache import bgcheckpoint


a, b = "This is A1", "This is B1"
a, b = bgcheckpoint('a', 'b', key="level1")
print("A: ", a)
print("B: ", b)

del a
del b

a, b = bgcheckpoint('a', 'b', key="level1")
print("A: ", a)
print("B: ", b)


# @bgcheckpoint()
# def function_to_checkpoint(cancer_type, verbose=False):
#     return "This is A2", "This is B2"
#
# a, b = function_to_checkpoint()
# print("A: ", a)
# print("B: ", b)



