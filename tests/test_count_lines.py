import gzip
import logging
import time
from multiprocessing.pool import Pool

from bgcache import bgcache


@bgcache
def count_lines(input_file):
    time.sleep(6)
    with gzip.open(input_file) as fd:
        return sum(1 for i in fd)


def count_file(filename):
    return count_lines(filename)


if __name__ == "__main__":
    # Configure the logging
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%H:%M:%S')
    logging.getLogger().setLevel(logging.INFO)

    t_ini = time.time()
    with Pool() as pool:
        for r in pool.imap_unordered(count_file, ['input01.tsv.gz']*10):
            assert r == 2264, "Invalid result"
            print(r)
    print(" (", time.time() - t_ini, ")")