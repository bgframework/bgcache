from setuptools import setup, find_packages


setup(
    name='bgcache',
    version="0.2",
    packages=find_packages(),
    install_requires=['dill']
)